#include <stdio.h>  
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

/* structures and types definitions */
typedef enum nodeType {leaf, OR, AND} nodetype;

typedef struct adtNode{
	nodetype type;				/* type of node */
	int number;					/* number of the node of this type */
	char name[15];				/* node name */
	int depth;					/* node depth */
	struct adtNode *parent;		/* parent */
	struct adtNode *children;	/* first child */
	struct adtNode *nextchild;	/* next sibling */ 
	struct adtNode *nextnode;	/* next node in the ADT */
} adtnode, *adtnodeptr;

/* functions defined in this program */
void createADT();
void printADT();
void writeImitatorFiles();
void writeFull(bool withdepth);
void writePattern(bool withdepth);

/* Global variables used for the characteristics of the generation */
int adtwidth = 0, adtdepth = 0, adtchildren = 0; /* parameters for the generation */
nodetype adttype = leaf;
char thefilename[20] = {'\0'}; /* the file to write */
adtnodeptr theADT = NULL; /* the first node of the ADT */
char cmdline[100];

/* main program */
int main(int argc, char *argv[])
{
	/* check the options passed */
    int opt, count;

    for (count = 0; count < argc; count++) {
    	strcat(cmdline,argv[count]);
    	strcat(cmdline," ");
    }
    while((opt = getopt(argc, argv, "w:d:c:f:t:")) != -1) {  
        switch(opt) {  
            case 'w': adtwidth = atoi(optarg); break;
            case 'd': adtdepth = atoi(optarg); break;
            case 'c': adtchildren = atoi(optarg); break;  
            case 't': if (!strcmp(optarg,"OR"))
            				adttype = OR;
            				else if (!strcmp(optarg,"AND"))
            						adttype = AND;
            						else {
            							printf("intermediate nodes must be of type AND or OR\n");
            							return 1;
            						}
            	break;  
            case 'f': strcpy(thefilename,optarg); break;  
            case ':': printf("option needs a value\n"); return 1;  
            case '?': printf("unknown option: %c\n", optopt); return 1;  
        }  
    }  
    // optind is for the extra arguments 
    // which are not parsed 
    for(; optind < argc; optind++){      
        printf("extra arguments: %s\n", argv[optind]);  
    }

    if (adtwidth == 0) {
    	printf("using default width 2\n");
    	adtwidth = 2;
    }
    if (adtdepth == 0) {
    	printf("using default depth 2\n");
    	adtdepth = 2;
    }
    if (adtchildren == 0) {
    	printf("using default of 2 children per node\n");
    	adtchildren = 2;
    }
    if (adttype == leaf){
    	printf("using default node type AND\n");
    	adttype = AND;
    }
    if (!strcmp(thefilename,"")) {
    	printf("A file name is needed\n");
    	return 1;
    }
    printf("Generating ADT of width %d, depth %d with type ",adtwidth, adtdepth);
    switch (adttype) {
    	case leaf : return 1;
    	case OR :  printf("OR"); break;
    	case AND :  printf("AND"); break;
    }
    printf(" and %d children in file %s\n", adtchildren, thefilename);
    createADT();
    printADT();
    writeImitatorFiles();
	return 0;
} /* end main */

/* create the ADT structure with the entry point in theADT global variable */
void createADT(){
	int lastleafnum = 0, lastnodenum = 0;
	int thedepth = adtdepth, thewidth, thechildren;
	adtnodeptr lastnode = NULL, thenode, thechild;
	bool morenodes;

	/* create the leaf nodes at maximum depth */
	for (thewidth = adtwidth; thewidth > 0 ; thewidth--) {
		/* create a leaf node */
		thenode = (adtnodeptr) malloc(sizeof(adtnode));
		thenode->type = leaf;
		lastleafnum++;
		thenode->number = lastleafnum;
		sprintf(thenode->name,"leaf%d",thenode->number);
		thenode->depth = thedepth;
		thenode->parent = NULL;
		thenode->children = NULL;
		thenode->nextchild = NULL;
		thenode->nextnode = NULL;
		if (theADT == NULL) {
			/* this is the first node */
			theADT = thenode;
			lastnode = thenode;
		} else {
			lastnode->nextnode = thenode;
			lastnode = thenode;
		}

	}
	/* create the intermediate nodes */
	for (thedepth--; thedepth >= 0; thedepth--){
		morenodes = true;
		while (morenodes) {
			thenode = (adtnodeptr) malloc(sizeof(adtnode));
			thenode->type = adttype;
			lastnodenum++;
			thenode->number = lastnodenum;
			if (thenode->type == OR)
				sprintf(thenode->name,"OR%d",thenode->number);
			else sprintf(thenode->name,"AND%d",thenode->number);
			thenode->depth = thedepth;
			thenode->parent = NULL;
			thenode->children = NULL;
			thenode->nextchild = NULL;
			thenode->nextnode = NULL;
			lastnode->nextnode = thenode;
			lastnode = thenode;
			/* find the children */
			thechild = theADT;
			for (thechildren = adtchildren; thechildren > 0; thechildren--){
				for (; thechild != NULL && (thechild->parent != NULL || thechild->depth != thedepth + 1);
					thechild = thechild->nextnode);
				if (thechild != NULL) {
					/* a child node was found */
					thechild->parent = thenode;
					thechild->nextchild = thenode->children;
					thenode->children = thechild;
				} else {
					/* no available children were found, create a new leaf */
					thechild = (adtnodeptr) malloc(sizeof(adtnode));
					thechild->type = leaf;
					lastleafnum++;
					thechild->number = lastleafnum;
					sprintf(thechild->name,"leaf%d",thechild->number);
					thechild->depth = thedepth + 1;
					thechild->parent = thenode;
					thechild->children = NULL;
					thechild->nextchild = thenode->children;
					thenode->children = thechild;
					thechild->nextnode = NULL;
					lastnode->nextnode = thechild;
					lastnode = thechild;	
				}
			}
			/* Are there more possible children at that depth? */
			for (; thechild != NULL && (thechild->parent != NULL || thechild->depth != thedepth + 1);
				thechild = thechild->nextnode);
			if (thechild == NULL)
				morenodes = false;
		}
	}
} /* end createADT */

/* print the ADT structure on the standard output */
void printADT(){
	adtnodeptr thenode = theADT, thechild;

	printf("Generated ADT:\n");
	while (thenode != NULL){
		printf("Node %s ", thenode->name);
		printf("at depth %d ", thenode->depth);
		if (thenode->parent == NULL)
			printf("No parent ");
		else printf("Parent %s ", (thenode->parent)->name);
		thechild = thenode->children;
		if (thechild == NULL)
			printf("No children");
		else printf("Children: ");
		for (;thechild != NULL; thechild = thechild->nextchild)
			printf("%s ", thechild->name);
		printf("\n");
		thenode = thenode->nextnode;
	}
}

/* Write the Imitator files */
void writeImitatorFiles(){
	writeFull(false);
	writeFull(true);
	writePattern(false);
	writePattern(true);
} /* end writeImitatorFile */

/* Write the Imitator files from the ADT structure */
void writeFull(bool withdepth){
	FILE *thefile;
	char filename[50];
	time_t current_time;
    char* c_time_string;
    adtnodeptr thenode, thechild1, thechild2;
    int locnum, thedepth, maxcount;

	strcpy(filename,thefilename);
	if (withdepth)
		strcat(filename,"_layer.imi");
	else strcat(filename,"_full.imi");
	thefile = fopen(filename,"w");
	/* write header */
	fprintf(thefile,"(* IMITATOR file automatically generated with *)\n");
	fprintf(thefile,"(* %s *)\n",cmdline);
	if (withdepth)
		fprintf(thefile,"(* full version with layers *)\n");
	else fprintf(thefile,"(* full version without reduction *)\n");
    current_time = time(NULL);
    c_time_string = strtok(ctime(&current_time), "\n");
    fprintf(thefile,"(* Created: %s *)\n\nvar\n", c_time_string);
    if (withdepth) {
    	thedepth = adtdepth;
    	/* number of nodes per depth */
    	for (thenode = theADT, maxcount = 0; thenode != NULL; thenode = thenode->nextnode)
    		if (thenode->depth == thedepth)
    			maxcount++;
    	fprintf(thefile,"\tmax_depth_%d = %d", thedepth, maxcount);
    	for (thedepth--; thedepth > 0; thedepth--) {
	    	for (thenode = theADT, maxcount = 0; thenode != NULL; thenode = thenode->nextnode)
	    		if (thenode->depth == thedepth)
	    			maxcount++;
    		fprintf(thefile,", max_depth_%d = %d", thedepth,maxcount);
    	}
    	fprintf(thefile," : constant;\n");
    	thedepth = adtdepth;
    	fprintf(thefile,"\tdepth_%d", thedepth);
    	for (thedepth--; thedepth > 0; thedepth--)
    		fprintf(thefile,", depth_%d", thedepth);
    	fprintf(thefile," : discrete;\n");
    }

    /* write the automata */
    for (thenode = theADT; thenode != NULL; thenode = thenode->nextnode){
	    fprintf(thefile,"\n(******************)\nautomaton %s\n(******************)\n",
	    	thenode->name);
	    /* message labels */
	    fprintf(thefile,"synclabs : %s_ok, %s_nok", thenode->name, thenode->name);
	    for (thechild1 = thenode->children; thechild1 != NULL; thechild1 = thechild1->nextchild)
	    	fprintf(thefile,", %s_ok, %s_nok", thechild1->name, thechild1->name);
	    fprintf(thefile,";\n");
	    /* locations and transitions */
	    switch (thenode->type){
	    	case leaf : /* leaf node */
			    fprintf(thefile,"loc l0 : while True wait {}\n");
				fprintf(thefile,"\twhen True sync %s_ok goto lok;\n", thenode->name);
				fprintf(thefile,"\twhen True sync %s_nok goto lnok;\n", thenode->name);
				fprintf(thefile,"loc lok : while True wait {}\n");
				fprintf(thefile,"loc lnok : while True wait {}\n");
	    		break;
			case OR : /* OR node */
	    		fprintf(thefile,"loc l0 : while True wait {}\n");
	    		locnum = 1;
	    		for (thechild1 = thenode->children;
	    			thechild1 != NULL;
	    			thechild1 = thechild1->nextchild) {
	    			fprintf(thefile,"\twhen True sync %s_ok goto l%d;\n", thechild1->name, locnum);
	    			fprintf(thefile,"\twhen True sync %s_nok goto ln%d;\n", thechild1->name, locnum);
	    		}
	    		for (; locnum < adtchildren; locnum++){
	    			fprintf(thefile,"loc l%d : while True wait {}\n", locnum);
		    		for (thechild1 = thenode->children;
		    			thechild1 != NULL;
		    			thechild1 = thechild1->nextchild) 
		    			if (withdepth && locnum == adtchildren - 1) {
			    			fprintf(thefile,"\twhen True sync %s_ok do {depth_%d' = depth_%d + %d} goto l%d;\n",
			    				thechild1->name, thechild1->depth, thechild1->depth,
			    				adtchildren, locnum + 1);
			    			fprintf(thefile,"\twhen True sync %s_nok do {depth_%d' = depth_%d + %d} goto l%d;\n",
			    				thechild1->name, thechild1->depth, thechild1->depth,
			    				adtchildren, locnum + 1);
		    			} else {
			    			fprintf(thefile,"\twhen True sync %s_ok goto l%d;\n",
			    				thechild1->name, locnum + 1);
			    			fprintf(thefile,"\twhen True sync %s_nok goto l%d;\n",
			    				thechild1->name, locnum + 1);
		    			}
	    			fprintf(thefile,"loc ln%d : while True wait {}\n", locnum);
		    		for (thechild1 = thenode->children;
		    			thechild1 != NULL;
		    			thechild1 = thechild1->nextchild)
		    			if (withdepth && locnum == adtchildren - 1) {
			    			fprintf(thefile,"\twhen True sync %s_ok do {depth_%d' = depth_%d + %d} goto l%d;\n",
			    				thechild1->name, thechild1->depth, thechild1->depth,
			    				adtchildren, locnum + 1);
			    			fprintf(thefile,"\twhen True sync %s_nok do {depth_%d' = depth_%d + %d} goto ln%d;\n",
			    				thechild1->name, thechild1->depth, thechild1->depth,
			    				adtchildren, locnum + 1);
		    			} else {
			    			fprintf(thefile,"\twhen True sync %s_ok goto l%d;\n",
			    				thechild1->name, locnum + 1);
			    			fprintf(thefile,"\twhen True sync %s_nok goto ln%d;\n",
			    				thechild1->name, locnum + 1);
		    			}
	    		}
	    		fprintf(thefile,"loc l%d : while True wait {}\n", locnum);
	    		if (withdepth)
	    			fprintf(thefile,"\twhen depth_%d = max_depth_%d sync %s_ok goto lok;\n",
	    				thenode->depth + 1, thenode->depth + 1, thenode->name);
	    		else fprintf(thefile,"\twhen True sync %s_ok goto lok;\n", thenode->name);
	    		fprintf(thefile,"loc ln%d : while True wait {}\n", locnum);
	    		if (withdepth)
	    			fprintf(thefile,"\twhen depth_%d = max_depth_%d sync %s_nok goto lnok;\n",
	    				thenode->depth + 1, thenode->depth + 1, thenode->name);
		    		else fprintf(thefile,"\twhen True sync %s_nok goto lnok;\n", thenode->name);
				fprintf(thefile,"loc lok : while True wait {}\n");
				fprintf(thefile,"loc lnok : while True wait {}\n");
			    break;
			case AND : /* AND node */
	    		fprintf(thefile,"loc l0 : while True wait {}\n");
	    		locnum = 1;
	    		for (thechild1 = thenode->children;
	    			thechild1 != NULL;
	    			thechild1 = thechild1->nextchild) {
	    			fprintf(thefile,"\twhen True sync %s_ok goto l%d;\n", thechild1->name, locnum);
	    			fprintf(thefile,"\twhen True sync %s_nok goto ln%d;\n", thechild1->name, locnum);
	    		}
	    		for (; locnum < adtchildren; locnum++){
	    			fprintf(thefile,"loc l%d : while True wait {}\n", locnum);
		    		for (thechild1 = thenode->children;
		    			thechild1 != NULL;
		    			thechild1 = thechild1->nextchild) 
		    			if (withdepth && locnum == adtchildren - 1) {
			    			fprintf(thefile,"\twhen True sync %s_ok do {depth_%d' = depth_%d + %d} goto l%d;\n",
			    				thechild1->name, thechild1->depth, thechild1->depth,
			    				adtchildren, locnum + 1);
			    			fprintf(thefile,"\twhen True sync %s_nok do {depth_%d' = depth_%d + %d} goto ln%d;\n",
			    				thechild1->name, thechild1->depth, thechild1->depth,
			    				adtchildren, locnum + 1);
		    			} else {
			    			fprintf(thefile,"\twhen True sync %s_ok goto l%d;\n",
			    				thechild1->name, locnum + 1);
			    			fprintf(thefile,"\twhen True sync %s_nok goto ln%d;\n",
			    				thechild1->name, locnum + 1);
		    			}
	    			fprintf(thefile,"loc ln%d : while True wait {}\n", locnum);
		    		for (thechild1 = thenode->children;
		    			thechild1 != NULL;
		    			thechild1 = thechild1->nextchild)
		    			if (withdepth && locnum == adtchildren - 1) {
			    			fprintf(thefile,"\twhen True sync %s_ok do {depth_%d' = depth_%d + %d} goto ln%d;\n",
			    				thechild1->name, thechild1->depth, thechild1->depth,
			    				adtchildren, locnum + 1);
			    			fprintf(thefile,"\twhen True sync %s_nok do {depth_%d' = depth_%d + %d} goto ln%d;\n",
			    				thechild1->name, thechild1->depth, thechild1->depth,
			    				adtchildren, locnum + 1);
		    			} else {
			    			fprintf(thefile,"\twhen True sync %s_ok goto ln%d;\n",
			    				thechild1->name, locnum + 1);
			    			fprintf(thefile,"\twhen True sync %s_nok goto ln%d;\n",
			    				thechild1->name, locnum + 1);
		    			}
	    		}
	    		fprintf(thefile,"loc l%d : while True wait {}\n", locnum);
	    		if (withdepth)
	    			fprintf(thefile,"\twhen depth_%d = max_depth_%d sync %s_ok goto lok;\n",
	    				thenode->depth + 1, thenode->depth + 1, thenode->name);
	    		else fprintf(thefile,"\twhen True sync %s_ok goto lok;\n", thenode->name);
	    		fprintf(thefile,"loc ln%d : while True wait {}\n", locnum);
	    		if (withdepth)
	    			fprintf(thefile,"\twhen depth_%d = max_depth_%d sync %s_nok goto lnok;\n",
	    				thenode->depth + 1, thenode->depth + 1, thenode->name);
		    		else fprintf(thefile,"\twhen True sync %s_nok goto lnok;\n", thenode->name);
				fprintf(thefile,"loc lok : while True wait {}\n");
				fprintf(thefile,"loc lnok : while True wait {}\n");
				break;
	    }
	    fprintf(thefile,"end\n");
	}

    /* write the initial state */
    fprintf(thefile,"\n(*****************)\n(* initial state *)\n(*****************)\ninit := ");
    thenode = theADT;
    fprintf(thefile,"loc[%s] = l0", thenode->name);
    thenode = thenode->nextnode;
    for (; thenode != NULL; thenode = thenode->nextnode)
	    fprintf(thefile," & loc[%s] = l0", thenode->name);
    if (withdepth) {
    	thedepth = adtdepth;
    	fprintf(thefile,"\n&\tdepth_%d = 0", thedepth);
    	for (thedepth--; thedepth > 0; thedepth--)
    		fprintf(thefile," & depth_%d = 0", thedepth);
    }
    fprintf(thefile,";\n");
	fclose(thefile);
} /* end writeImitatorFile */

/* Write the Imitator files from the ADT structure */
void writePattern(bool withdepth){
	FILE *thefile;
	char filename[50];
	time_t current_time;
    char* c_time_string;
    adtnodeptr thenode, thechild1, thechild2;
    int locnum, thedepth, maxcount;

	strcpy(filename,thefilename);
	if (withdepth)
		strcat(filename,"_both.imi");
	else strcat(filename,"_pattern.imi");
	thefile = fopen(filename,"w");
	/* write header */
	fprintf(thefile,"(* IMITATOR file automatically generated with *)\n");
	fprintf(thefile,"(* %s *)\n",cmdline);
	if (withdepth)
		fprintf(thefile,"(* version with both patterns and layers *)\n");
	else fprintf(thefile,"(* version with patterns only *)\n");
    current_time = time(NULL);
    c_time_string = strtok(ctime(&current_time), "\n");
    fprintf(thefile,"(* Created: %s *)\n\nvar\n", c_time_string);
    if (withdepth) {
    	thedepth = adtdepth;
    	/* number of nodes per depth */
    	for (thenode = theADT, maxcount = 0; thenode != NULL; thenode = thenode->nextnode)
    		if (thenode->depth == thedepth)
    			maxcount++;
    	fprintf(thefile,"\tmax_depth_%d = %d", thedepth, maxcount);
    	for (thedepth--; thedepth > 0; thedepth--) {
	    	for (thenode = theADT, maxcount = 0; thenode != NULL; thenode = thenode->nextnode)
	    		if (thenode->depth == thedepth)
	    			maxcount++;
    		fprintf(thefile,", max_depth_%d = %d", thedepth,maxcount);
    	}
    	fprintf(thefile," : constant;\n");
    	thedepth = adtdepth;
    	fprintf(thefile,"\tdepth_%d", thedepth);
    	for (thedepth--; thedepth > 0; thedepth--)
    		fprintf(thefile,", depth_%d", thedepth);
    	fprintf(thefile," : discrete;\n");
    }

    /* write the automata */
    for (thenode = theADT; thenode != NULL; thenode = thenode->nextnode){
	    fprintf(thefile,"\n(******************)\nautomaton %s\n(******************)\n",
	    	thenode->name);
	    /* message labels */
	    fprintf(thefile,"synclabs : %s_ok, %s_nok", thenode->name, thenode->name);
	    for (thechild1 = thenode->children; thechild1 != NULL; thechild1 = thechild1->nextchild)
	    	fprintf(thefile,", %s_ok, %s_nok", thechild1->name, thechild1->name);
	    fprintf(thefile,";\n");
	    /* locations and transitions */
	    switch (thenode->type){
	    	case leaf : /* leaf node */
			    fprintf(thefile,"loc l0 : while True wait {}\n");
				fprintf(thefile,"\twhen True sync %s_ok goto lok;\n", thenode->name);
				fprintf(thefile,"\twhen True sync %s_nok goto lnok;\n", thenode->name);
				fprintf(thefile,"loc lok : while True wait {}\n");
				fprintf(thefile,"loc lnok : while True wait {}\n");
	    		break;
			case OR : /* OR node */
			    fprintf(thefile,"loc l0 : while True wait {}\n");
			    thechild1 = thenode->children;
			    locnum = 1;
				fprintf(thefile,"\twhen True sync %s_nok goto l%d;\n", thechild1->name, locnum);
				for (thechild2 = thechild1;
						thechild2 != NULL;
						thechild2 = thechild2->nextchild) {
					fprintf(thefile,"\twhen True sync %s_ok", thechild2->name);
					if (withdepth)
						fprintf(thefile," do {depth_%d' = depth_%d + %d}",
							thechild2->depth, thechild2->depth, adtchildren);
					fprintf(thefile," goto lf;\n");
				}
				for (thechild2 = thechild1->nextchild; thechild2 != NULL;
						thechild2 = thechild2->nextchild, locnum++)
					if (thechild2->nextchild != NULL)
						fprintf(thefile,
							"loc l%d : while True wait {}\n\twhen True sync %s_nok goto l%d;\n",
							locnum, thechild2->name, locnum + 1);
					else if (withdepth)
							fprintf(thefile,
								"loc l%d : while True wait {}\n\twhen True sync %s_nok do {depth_%d' = depth_%d + %d} goto l%d;\n",
								locnum, thechild2->name, thechild2->depth, thechild2->depth,
								adtchildren, locnum + 1);
						else fprintf(thefile,
								"loc l%d : while True wait {}\n\twhen True sync %s_nok goto l%d;\n",
								locnum, thechild2->name, locnum + 1);
				fprintf(thefile,"loc l%d : while True wait {}\n\twhen ", locnum);
				if (withdepth)
					fprintf(thefile,"depth_%d = max_depth_%d", thenode->depth + 1, thenode->depth + 1);
				else fprintf(thefile,"True");
				fprintf(thefile," sync %s_nok goto lnok;\n", thenode->name);
				fprintf(thefile,"loc lf : while True wait {}\n\twhen ");
				if (withdepth)
					fprintf(thefile,"depth_%d = max_depth_%d", thenode->depth + 1, thenode->depth + 1);
				else fprintf(thefile,"True");
				fprintf(thefile," sync %s_ok goto lok;\n", thenode->name);
				fprintf(thefile,"loc lok : while True wait {}\n");
				fprintf(thefile,"loc lnok : while True wait {}\n");
			    break;
			case AND : /* AND node */
			    fprintf(thefile,"loc l0 : while True wait {}\n");
			    thechild1 = thenode->children;
			    locnum = 1;
				fprintf(thefile,"\twhen True sync %s_ok goto l%d;\n", thechild1->name, locnum);
				for (thechild2 = thechild1;
						thechild2 != NULL;
						thechild2 = thechild2->nextchild) {
					fprintf(thefile,"\twhen True sync %s_nok", thechild2->name);
					if (withdepth)
						fprintf(thefile," do {depth_%d' = depth_%d + %d}",
							thechild2->depth, thechild2->depth, adtchildren);
					fprintf(thefile," goto lf;\n");
				}
				for (thechild2 = thechild1->nextchild; thechild2 != NULL;
						thechild2 = thechild2->nextchild, locnum++)
					if (thechild2->nextchild != NULL)
						fprintf(thefile,
							"loc l%d : while True wait {}\n\twhen True sync %s_ok goto l%d;\n",
							locnum, thechild2->name, locnum + 1);
					else if (withdepth)
							fprintf(thefile,
								"loc l%d : while True wait {}\n\twhen True sync %s_ok do {depth_%d' = depth_%d + %d} goto l%d;\n",
								locnum, thechild2->name, thechild2->depth, thechild2->depth,
								adtchildren, locnum + 1);
						else fprintf(thefile,
								"loc l%d : while True wait {}\n\twhen True sync %s_ok goto l%d;\n",
								locnum, thechild2->name, locnum + 1);
				fprintf(thefile,"loc l%d : while True wait {}\n\twhen ", locnum);
				if (withdepth)
					fprintf(thefile,"depth_%d = max_depth_%d", thenode->depth + 1, thenode->depth + 1);
				else fprintf(thefile,"True");
				fprintf(thefile," sync %s_ok goto lok;\n", thenode->name);
				fprintf(thefile,"loc lf : while True wait {}\n\twhen ");
				if (withdepth)
					fprintf(thefile,"depth_%d = max_depth_%d", thenode->depth + 1, thenode->depth + 1);
				else fprintf(thefile,"True");
				fprintf(thefile," sync %s_nok goto lnok;\n", thenode->name);
				fprintf(thefile,"loc lok : while True wait {}\n");
				fprintf(thefile,"loc lnok : while True wait {}\n");
				break;
	    }
	    fprintf(thefile,"end\n");
	}

    /* write the initial state */
    fprintf(thefile,"\n(*****************)\n(* initial state *)\n(*****************)\ninit := ");
    thenode = theADT;
    fprintf(thefile,"loc[%s] = l0", thenode->name);
    thenode = thenode->nextnode;
    for (; thenode != NULL; thenode = thenode->nextnode)
	    fprintf(thefile," & loc[%s] = l0", thenode->name);
    if (withdepth) {
    	thedepth = adtdepth;
    	fprintf(thefile,"\n&\tdepth_%d = 0", thedepth);
    	for (thedepth--; thedepth > 0; thedepth--)
    		fprintf(thefile," & depth_%d = 0", thedepth);
    }
    fprintf(thefile,";\n");
	fclose(thefile);
} /* end writeImitatorFile */
