(************************************************************
 *                      IMITATOR MODEL                      
 *
 * Iot-dev Attack Defence Tree
 *
 * Description     : Attack Defence Tree
 * Author          : Laure Petrucci
 *
 * Created         : 2018/12/04
 * Last modified   : 2018/12/04
 *
 * IMITATOR version: 2.10.1
 ************************************************************)

var

(* clocks *)

(* constants *)
	init_cost_flp = 10, init_cost_sma = 50, init_cost_fw = 10,
	init_cost_bwk = 100, init_cost_gc = 100, init_cost_tla = 5,
	init_cost_inc = 5, init_cost_esv = 10, init_cost_rms = 100,
	init_cost_AL = 0, init_cost_AW = 0, init_cost_CPN = 0,
	init_cost_GVC = 0, init_cost_APN = 0, init_cost_APNS = 0,
	init_cost_CIoTD = 0,
	init_time_flp = 60, init_time_sma = 30, init_time_fw = 300,
	init_time_bwk = 120, init_time_gc = 600, init_time_tla = 1,
	init_time_inc = 1, init_time_esv = 60, init_time_rms = 30,
	init_time_AL = 0, init_time_AW = 0, init_time_CPN = 0,
	init_time_GVC = 0, init_time_APN = 3, init_time_APNS = 1,
	init_time_CIoTD = 0
		: constant;

(* discrete variables *)
	cost_AL, cost_AW, cost_CPN,
	cost_GVC, cost_APN, cost_APNS, cost_CIoTD,
	time_AL, time_AW, time_CPN,
	time_GVC, time_APN, time_APNS, time_CIoTD,
	TLE_timeA, TLE_timeD, TLE_costA, TLE_costD,
	choice_AL, choice_AW, choice_CPN, choice_GVC, choice_APNS,
	choice_CIoTD
		: discrete;

(* parameters *)
(*	 init_time_p : parameter;*)

(******************)
automaton flp
(******************)
synclabs : flp_ok, flp_nok;
loc l0 : while True wait {}
	when True sync flp_ok
		do {TLE_timeA' = TLE_timeA + init_time_flp,
			TLE_costA' = TLE_costA + init_cost_flp} goto lok;
	when True sync flp_nok goto lnok;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton sma
(******************)
synclabs : sma_ok, sma_nok;
loc l0 : while True wait {}
	when True sync sma_ok
		do {TLE_timeA' = TLE_timeA + init_time_sma,
			TLE_costA' = TLE_costA + init_cost_sma} goto lok;
	when True sync sma_nok goto lnok;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton fw
(******************)
synclabs : fw_ok, fw_nok;
loc l0 : while True wait {}
	when True sync fw_ok
		do {TLE_timeA' = TLE_timeA + init_time_fw,
			TLE_costA' = TLE_costA + init_cost_fw} goto lok;
	when True sync fw_nok goto lnok;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton bwk
(******************)
synclabs : bwk_ok, bwk_nok;
loc l0 : while True wait {}
	when True sync bwk_ok
		do {TLE_timeA' = TLE_timeA + init_time_bwk,
			TLE_costA' = TLE_costA + init_cost_bwk} goto lok;
	when True sync bwk_nok goto lnok;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton gc
(******************)
synclabs : gc_ok, gc_nok;
loc l0 : while True wait {}
	when True sync gc_ok
		do {TLE_timeA' = TLE_timeA + init_time_gc,
			TLE_costA' = TLE_costA + init_cost_gc} goto lok;
	when True sync gc_nok goto lnok;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton tla
(******************)
synclabs : tla_ok, tla_nok;
loc l0 : while True wait {}
	when True sync tla_ok
		do {TLE_timeD' = TLE_timeD + init_time_tla,
			TLE_costD' = TLE_costD + init_cost_tla} goto lok;
	when True sync tla_nok goto lnok;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton inc
(******************)
synclabs : inc_ok, inc_nok;
loc l0 : while True wait {}
	when True sync inc_ok
		do {TLE_timeD' = TLE_timeD + init_time_inc,
			TLE_costD' = TLE_costD + init_cost_inc} goto lok;
	when True sync inc_nok goto lnok;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton esv
(******************)
synclabs : esv_ok, esv_nok;
loc l0 : while True wait {}
	when True sync esv_ok
		do {TLE_timeA' = TLE_timeA + init_time_esv,
			TLE_costA' = TLE_costA + init_cost_esv} goto lok;
	when True sync esv_nok goto lnok;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton rms
(******************)
synclabs : rms_ok, rms_nok;
loc l0 : while True wait {}
	when True sync rms_ok
		do {TLE_timeA' = TLE_timeA + init_time_rms,
			TLE_costA' = TLE_costA + init_cost_rms} goto lok;
	when True sync rms_nok goto lnok;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton AL
(******************)
synclabs : AL_ok, AL_nok, flp_ok, flp_nok, sma_ok, sma_nok;
loc l0 : while True wait {}
	when True sync flp_ok goto l1;
	when True sync flp_nok
		do {choice_AL' = 1} goto l4;
loc l1 : while True wait {}
	when True sync sma_ok goto l2;
	when True sync sma_nok
		do {choice_AL' = 2} goto l3;
loc l2 : while True wait {}
	when True sync AL_ok
		do {cost_AL' = init_cost_AL + init_cost_flp + init_cost_sma,
			time_AL' = init_time_AL + init_time_flp + init_time_sma,
			TLE_timeA' = TLE_timeA + init_time_AL,
			TLE_costA' = TLE_costA + init_cost_AL} goto lok;
loc l3 : while True wait {}
	when choice_AL = 1 sync AL_nok goto lnok;
	when choice_AL = 2 sync AL_nok
		do {cost_AL' = init_cost_flp,
			time_AL' = init_time_flp} goto lnok;
loc l4 : while True wait {}
	when True sync sma_ok goto l3;
	when True sync sma_nok goto l3;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton AW
(******************)
synclabs : AW_ok, AW_nok, fw_ok, fw_nok, bwk_ok, bwk_nok;
loc l0 : while True wait {}
	when True sync fw_ok goto l1;
	when True sync fw_nok
		do {choice_AW' = 1} goto l4;
loc l1 : while True wait {}
	when True sync bwk_ok goto l2;
	when True sync bwk_nok
		do {choice_AW' = 2} goto l3;
loc l2 : while True wait {}
	when True sync AW_ok
		do {cost_AW' = init_cost_AW + init_cost_fw + init_cost_bwk,
			time_AW' = init_time_AW + init_time_fw + init_time_bwk,
			TLE_timeA' = TLE_timeA + init_time_AW,
			TLE_costA' = TLE_costA + init_cost_AW} goto lok;
loc l3 : while True wait {}
	when choice_AW = 1 sync AW_nok goto lnok;
	when choice_AW = 2 sync AW_nok
		do {cost_AW' = init_cost_fw,
			time_AW' = init_time_fw} goto lnok;
loc l4 : while True wait {}
	when True sync bwk_ok goto l3;
	when True sync bwk_nok goto l3;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton CPN
(******************)
synclabs : CPN_ok, CPN_nok, AL_ok, AL_nok, AW_ok, AW_nok;
loc l0 : while True wait {}
	when True sync AL_nok goto l1;
	when True sync AL_ok
		do {choice_CPN' = 1} goto l4;
	when True sync AW_nok goto l1;
	when True sync AW_ok
		do {choice_CPN' = 2} goto l4;
loc l1 : while True wait {}
	when True sync AL_nok goto l2;
	when True sync AL_ok
		do {choice_CPN' = 1} goto l3;
	when True sync AW_nok goto l2;
	when True sync AW_ok
		do {choice_CPN' = 2} goto l3;
loc l2 : while True wait {}
	when True sync CPN_nok goto lnok;
loc l3 : while True wait {}
	when choice_CPN = 1 sync CPN_ok
		do {cost_CPN' = init_cost_CPN + cost_AL,
			time_CPN' = init_time_CPN + time_AL,
			TLE_timeA' = TLE_timeA + init_time_CPN,
			TLE_costA' = TLE_costA + init_cost_CPN} goto lok;
	when choice_CPN = 2 sync CPN_ok
		do {cost_CPN' = init_cost_CPN + cost_AW,
			time_CPN' = init_time_CPN + time_AW,
			TLE_timeA' = TLE_timeA + init_time_CPN,
			TLE_costA' = TLE_costA + init_cost_CPN} goto lok;
loc l4 : while True wait {}
	when True sync AL_nok goto l3;
	when True sync AL_ok goto l3;
	when True sync AW_nok goto l3;
	when True sync AW_ok goto l3;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton GVC
(******************)
synclabs : GVC_ok, GVC_nok, gc_ok, gc_nok, tla_ok, tla_nok;
loc l0 : while True wait {}
	when True sync gc_ok goto l1;
	when True sync gc_nok
		do {choice_GVC' = 1} goto l6;
	when True sync tla_nok goto l4;
	when True sync tla_ok
		do {choice_GVC' = 2} goto l5;
loc l1 : while True wait {}
	when True sync tla_nok goto l2;
	when True sync tla_ok goto l3;
loc l2 : while True wait {}
	when True sync GVC_ok
		do {cost_GVC' = init_cost_GVC + init_cost_gc,
			time_GVC' = init_time_GVC + init_time_gc,
			TLE_timeA' = TLE_timeA + init_time_GVC,
			TLE_costA' = TLE_costA + init_cost_GVC} goto lok;
loc l3 : while True wait {}
	when choice_GVC = 1 sync GVC_nok goto lnok;
	when choice_GVC = 2 sync GVC_nok
		do {cost_GVC' = init_cost_tla,
			time_GVC' = init_time_tla} goto lnok;
loc l4 : while True wait {}
	when True sync gc_ok goto l2;
	when True sync gc_nok
		do {choice_GVC' = 1} goto l3;
loc l5 : while True wait {}
	when True sync gc_ok goto l3;
	when True sync gc_nok goto l3;
loc l6 : while True wait {}
	when True sync tla_ok goto l3;
	when True sync tla_nok goto l3;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton APN
(******************)
synclabs : APN_ok, APN_nok, CPN_ok, CPN_nok, GVC_ok, GVC_nok;
loc l0 : while True wait {}
	when True sync CPN_ok goto l1;
	when True sync CPN_nok goto l4;
	when True sync GVC_ok goto l5;
	when True sync GVC_nok goto l6;
loc l1 : while True wait {}
	when True sync GVC_ok goto l2;
	when True sync GVC_nok goto l3;
loc l2 : while True wait {}
	when True sync APN_ok
		do {cost_APN' = init_cost_APN + cost_CPN + cost_GVC,
			time_APN' = init_time_APN + time_CPN + time_GVC,
			TLE_timeA' = TLE_timeA + init_time_APN,
			TLE_costA' = TLE_costA + init_cost_APN} goto lok;
loc l3 : while True wait {}
	when True sync APN_nok goto lnok;
loc l4 : while True wait {}
	when True sync GVC_ok goto l3;
	when True sync GVC_nok goto l3;
loc l5 : while True wait {}
	when True sync CPN_ok goto l2;
	when True sync CPN_nok goto l3;
loc l6 : while True wait {}
	when True sync CPN_ok goto l3;
	when True sync CPN_nok goto l3;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton APNS
(******************)
synclabs : APNS_ok, APNS_nok, APN_ok, APN_nok, inc_ok, inc_nok;
loc l0 : while True wait {}
	when True sync APN_ok goto l1;
	when True sync APN_nok
		do {choice_APNS' = 1} goto l4;
loc l1 : while True wait {}
	when True sync inc_nok goto l2;
	when True sync inc_ok
		do {choice_APNS' = 2} goto l3;
loc l2 : while True wait {}
	when True sync APNS_ok
		do {cost_APNS' = init_cost_APNS + cost_APN,
			time_APNS' = init_time_APNS + time_APN,
			TLE_timeA' = TLE_timeA + init_time_APNS,
			TLE_costA' = TLE_costA + init_cost_APNS} goto lok;
loc l3 : while True wait {}
	when choice_APNS = 1 sync APNS_nok goto lnok;
	when choice_APNS = 2 sync APNS_nok
		do {cost_APNS' = init_cost_inc,
			time_APNS' = init_time_inc + time_APN} goto lnok;
loc l4 : while True wait {}
	when True sync inc_nok goto l3;
	when True sync inc_ok goto l3;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton CIoTD
(******************)
synclabs : CIoTD_ok, CIoTD_nok, APNS_ok, APNS_nok,
	esv_ok, esv_nok, rms_ok, rms_nok;
loc l0 : while True wait {}
	when True sync APNS_ok goto l1;
	when True sync APNS_nok
		do {choice_CIoTD' = 1} goto l5;
loc l1 : while True wait {}
	when True sync esv_ok goto l2;
	when True sync esv_nok
		do {choice_CIoTD' = 2} goto l6;
loc l2 : while True wait {}
	when True sync rms_ok goto l3;
	when True sync rms_nok
		do {choice_CIoTD' = 3} goto l4;
loc l3 : while True wait {}
	when True sync CIoTD_ok
		do {cost_CIoTD' = init_cost_CIoTD + cost_APNS + init_cost_esv + init_cost_rms,
			time_CIoTD' = init_time_CIoTD + time_APNS + init_time_esv + init_time_rms,
			TLE_timeA' = TLE_timeA + init_time_CIoTD,
			TLE_costA' = TLE_costA + init_cost_CIoTD} goto lok;
loc l4 : while True wait {}
	when choice_CIoTD = 1 sync CIoTD_nok goto lnok;
	when choice_CIoTD = 2 sync CIoTD_nok
		do {cost_CIoTD' = cost_APNS,
			time_CIoTD' = time_APNS} goto lnok;
	when choice_CIoTD = 3 sync CIoTD_nok
		do {cost_CIoTD' = cost_APNS + init_cost_esv,
			time_CIoTD' = time_APNS + init_time_esv} goto lnok;
loc l5 : while True wait {}
	when True sync esv_ok goto l6;
	when True sync esv_nok goto l6;
loc l6 : while True wait {}
	when True sync rms_ok goto l4;
	when True sync rms_nok goto l4;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton TLE (* toplevel *)
(******************)
synclabs : CIoTD_ok, CIoTD_nok;
loc l0 : while True wait {}
	when True sync CIoTD_ok goto lok;
	when True sync CIoTD_nok goto lnok;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(*****************)
(* initial state *)
(*****************)
init :=
	(* location *)
	loc[flp] = l0 & loc[sma] = l0 & loc[fw] = l0 & loc[bwk] = l0
&	loc[gc] = l0 & loc[tla] = l0 & loc[inc] = l0 & loc[esv] = l0
&	loc[rms] = l0 & loc[AL] = l0 & loc[AW] = l0 & loc[CPN] = l0
&	loc[GVC] = l0 & loc[APN] = l0 & loc[APNS] = l0 & loc[CIoTD] = l0
&	loc[TLE] = l0
	(* discrete variables *)
&	cost_AL = 0 & cost_AW = 0 & cost_CPN = 0
&	cost_GVC = 0 & cost_APN = 0 & cost_APNS = 0 & cost_CIoTD = 0
&	time_AL = 0 & time_AW = 0 & time_CPN = 0
&	time_GVC = 0 & time_APN = 0 & time_APNS = 0 & time_CIoTD = 0
&	TLE_timeA = 0 & TLE_timeD = 0 & TLE_costA = 0 & TLE_costD = 0
&	choice_AL = 0 & choice_AW = 0 & choice_CPN = 0 & choice_GVC = 0
&	choice_APNS = 0 & choice_CIoTD = 0
	(* parameters *)
(* & init_time_p > 0 *)
;

(************)
(* property *)
(************)
property := unreachable loc[TLE] = lok;
